package com.bca.bia.util;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class BCAUtil {

    private final Base64 base64 = new Base64();

    public String getBase64Auth(String clientID, String clientSecret) {
        String encodedVersion = new String(base64.encode((clientID + ":" + clientSecret).getBytes()));
        return "Basic " + encodedVersion;
    }

    public String getBCASignature(
            String apiSecret,
            String HTTPMethod,
            String relativeURI,
            TreeMap<String, String> queryParams,
            String accessToken,
            String payload,
            String timestamp) throws Exception {
        
        String HMACAlgorithm = "HmacSHA256";
        SecretKey secKey = new SecretKeySpec(apiSecret.getBytes("UTF-8"), HMACAlgorithm);
        Mac hmac = Mac.getInstance(HMACAlgorithm);
        hmac.init(secKey);
        String sortedQueryParams = "";
        if (queryParams != null) {
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                String key = entry.getKey();
                sortedQueryParams = sortedQueryParams + key + "=" + entry.getValue() + "&";
            }
            if (sortedQueryParams.length() > 0) {
                sortedQueryParams = "?" + sortedQueryParams.substring(0, sortedQueryParams.length() - 1);
            }
        }
        if (payload == null) {
            payload = "";
        }
        payload = payload.replaceAll("\\s+", "");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(payload.getBytes("UTF-8"));
        return new String(Hex.encodeHex(hmac.doFinal((HTTPMethod + ":" + relativeURI + sortedQueryParams + ":" + accessToken + ":" + new String(Hex.encodeHex(md.digest())).toLowerCase() + ":" + timestamp).getBytes("UTF-8"))));
    }

    public String getDateTime() {
        TimeZone tz = TimeZone.getTimeZone("GMT+7");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
            @Override
            public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
                StringBuffer rfcFormat = super.format(date, toAppendTo, pos);
                return rfcFormat.insert(rfcFormat.length() - 2, ":");
            }

            @Override
            public Date parse(String text, ParsePosition pos) {
                if (text.length() > 3) {
                    text = text.substring(0, text.length() - 3) + text.substring(text.length() - 2);
                }
                return super.parse(text, pos);
            }
        };
        df.setTimeZone(tz);
        return df.format(new Date());
    }

}
