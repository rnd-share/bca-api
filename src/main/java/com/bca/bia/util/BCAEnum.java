package com.bca.bia.util;

public interface BCAEnum {

    public interface SIGNATURE {

        public static final String ONEKLIK = "signature/oneklik";
    }
    
    public interface METHOD {
        public static final String GET = "GET";
        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String DELETE = "DELETE";
        
    }

    public static final String URL = "https://sandbox.bca.co.id";
    public static int TIMEOUT = 60;
}
