package com.bca.bia.util;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.TreeMap;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

public class ServiceHandler {

    private final OkHttpClient client;
    private final BCAUtil bcaUtil;

    public ServiceHandler() {
        client = new OkHttpClient();
        bcaUtil = new BCAUtil();
    }

    public String getToken(String url, String clientID, String clientSecret) throws IOException {

        RequestBody body = new FormBody.Builder()
                .add("grant_type", "client_credentials")
                .build();

        System.out.println("base64 : " + bcaUtil.getBase64Auth(clientID, clientSecret));

        Request request = new Request.Builder()
                .url(BCAEnum.URL + url)
                .header("Authorization", bcaUtil.getBase64Auth(clientID, clientSecret))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(body)
                .build();

        Response response = client.newCall(request).execute();

        String string = null;
        try {
            string = response.body().string();
            return string;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (response != null) {
                response.close();
            }
            return string;
        }

    }

    public String method(Authorization authorization, String method, String url) throws ConnectException, UnknownHostException, SocketTimeoutException, IOException, Exception {
        return method(authorization, method, url, null, null);
    }

    public String method(Authorization authorization, String method, String url, TreeMap<String, String> queryParam) throws ConnectException, UnknownHostException, SocketTimeoutException, IOException, Exception {
        return method(authorization, method, url, null, queryParam);
    }

    public String method(Authorization authorization, String method, String url, String payload) throws ConnectException, UnknownHostException, SocketTimeoutException, IOException, Exception {
        return method(authorization, method, url, payload, null);
    }

    public String method(Authorization authorization, String method, String url, String payload, TreeMap<String, String> queryParams) throws ConnectException, UnknownHostException, SocketTimeoutException, IOException, Exception {
        String dateTime = bcaUtil.getDateTime();
        String signature;
        Request request;

        if (payload != null && queryParams == null) {
            signature = bcaUtil.getBCASignature(
                    authorization.getApiSecret(),
                    method,
                    url,
                    null,
                    authorization.getAccessToken(),
                    payload,
                    dateTime);
        } else if (payload == null && queryParams != null) {
            signature = bcaUtil.getBCASignature(
                    authorization.getApiSecret(),
                    method,
                    url,
                    queryParams,
                    authorization.getAccessToken(),
                    null,
                    dateTime);

        } else if (payload != null && queryParams != null) {
            signature = bcaUtil.getBCASignature(
                    authorization.getApiSecret(),
                    method,
                    url,
                    queryParams,
                    authorization.getAccessToken(),
                    payload,
                    dateTime);

        } else {
            signature = bcaUtil.getBCASignature(
                    authorization.getApiSecret(),
                    method,
                    url,
                    null,
                    authorization.getAccessToken(),
                    null,
                    dateTime);
        }

        String sortedQueryParams = "";
        if (queryParams != null) {
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {
                String key = entry.getKey();
                sortedQueryParams = sortedQueryParams + key + "=" + entry.getValue() + "&";
            }
            if (sortedQueryParams.length() > 0) {
                sortedQueryParams = "?" + sortedQueryParams.substring(0, sortedQueryParams.length() - 1);
            }
        }

        url = url + sortedQueryParams;

        System.out.println("URL                       : " + url);

        if (method.equals(BCAEnum.METHOD.POST)) {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            RequestBody requestBody = RequestBody.create(JSON, payload);

            System.out.println("BCA_API - Body            : " + bodyToString(requestBody));

            request = new Request.Builder()
                    .header("Authorization", "Bearer " + authorization.getAccessToken())
                    .header("Content-Type", "application/json")
                    .header("X-BCA-Key", authorization.getApiKey())
                    .header("X-BCA-Timestamp", dateTime)
                    .header("X-BCA-Signature", signature)
                    .post(requestBody)
                    .url(BCAEnum.URL + url)
                    .build();
        } else {

            request = new Request.Builder()
                    .header("Authorization", "Bearer " + authorization.getAccessToken())
                    .header("Content-Type", "application/json")
                    .header("X-BCA-Key", authorization.getApiKey())
                    .header("X-BCA-Timestamp", dateTime)
                    .header("X-BCA-Signature", signature)
                    .get()
                    .url(BCAEnum.URL + url)
                    .build();
        }

        try {
            System.out.println("BCA_API - Token           : Bearer " + authorization.getAccessToken());
            System.out.println("BCA_API - X-BCA-Key       : " + authorization.getApiKey());
            System.out.println("BCA_API - X-BCA-Timestamp : " + dateTime);
            System.out.println("BCA_API - X-BCA-Signature : " + signature);
        } catch (Exception e) {
        }
        Response response = this.client.newCall(request).execute();

        String string = null;
        try {
            string = response.body().string();
        } catch (ConnectException ex) {
            ex.printStackTrace();
            throw new ConnectException();
        } catch (SocketException ex) {
            ex.printStackTrace();
            throw new SocketException();
        } catch (SocketTimeoutException ex) {
            ex.printStackTrace();
            throw new SocketTimeoutException();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            if (response != null) {
                response.close();
            }
            return string;
        }
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

}
