/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bca.bia.util;

import java.util.TreeMap;
import org.json.JSONObject;

/**
 *
 * @author Fandy Adam
 */
public class Main {

    private final String apiKey = "6bdb210a-7d0e-47bf-a2db-0a84de3bb145";
    private final String apiSecret = "f46c17e5-9c1f-4132-831a-5ba408a1062f";
    private final String clientID = "3908362a-9a6d-4d83-8b7d-ae2aa6818190";
    private final String clientSecret = "75665e35-eae6-4892-8828-a117da9c31fb";

    public static void main(String[] args) {

        try {
            new Main().run();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void run() throws Exception {
        ServiceHandler handler = new ServiceHandler();
        //get access token
        String tempAccessToken = handler.getToken("/api/oauth/token", clientID, clientSecret);
        JSONObject jsonAccessToken = new JSONObject(tempAccessToken);
        String accessToken = jsonAccessToken.getString("access_token");
        System.out.println("accessToken : " + accessToken);

        Authorization authorization = new Authorization();
        authorization.setAccessToken(accessToken);
        authorization.setApiKey(apiKey);
        authorization.setApiSecret(apiSecret);
        authorization.setClientID(clientID);
        authorization.setClientSecret(clientSecret);

        try {
            //get balance
            //https://developer.bca.co.id/documentation/?http#business-banking
            String response = handler.method(authorization, BCAEnum.METHOD.GET, "/banking/v3/corporates/BCAAPI2016/accounts/0201245680");
            System.out.println("response : " + response + "\n\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            //Account Statement
            TreeMap<String, String> queryParam = new TreeMap<>();
            queryParam.put("StartDate", "2016-08-29");
            queryParam.put("EndDate", "2016-09-01");

            String response = handler.method(authorization, BCAEnum.METHOD.GET, "/banking/v3/corporates/BCAAPI2016/accounts/0201245680/statements", queryParam);
            System.out.println("response : " + response + "\n\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("CorporateID", "BCAAPI2016");
            jSONObject.put("SourceAccountNumber", "0201245680");
            jSONObject.put("TransactionID", "00000001");
            jSONObject.put("TransactionDate", "2016-01-30");
            jSONObject.put("ReferenceID", "12345/PO/2016");
            jSONObject.put("CurrencyCode", "IDR");
            jSONObject.put("Amount", "100000");
            jSONObject.put("BeneficiaryAccountNumber", "0201245681");
            jSONObject.put("Remark1", "Transfer Test");
            jSONObject.put("Remark2", "Online Transfer");

            //Fund Transfer
            String response = handler.method(authorization, BCAEnum.METHOD.POST, "/banking/corporates/transfers", jSONObject.toString());
            System.out.println("response : " + response + "\n\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
